<h1 class="mt-4">${vorgang.bezeichnung}</h1>

<!-- Preview Image -->
<g:if test="${(vorgang.bilder)}">
    <img style="max-height: 500px;" class="img-fluid rounded"
         src="/dateiAnhang/render/${vorgang.bilder.sort { it.id }.last().id}" alt="">
    <hr>
</g:if>


<!-- Post Content -->
<% if (vorgang.beschreibung) { %>
<h2>Beschreibung</h2>
${vorgang.beschreibung.encodeAsRaw()}
<% } %>

<!-- Post Content -->
<% if (vorgang.antragsText) { %>
<hr>
<div class="card card-body bg-light">
    <% if (vorgang.antragEingereichtAm) { %>
<h2>Antragstext vom ${vorgang.antragEingereichtAm.format('dd.MM.yyyy')}</h2>
    <% }else{ %>
<h2>Antragsentwurf</h2>
    <% } %>
${vorgang.antragsText.encodeAsRaw()}
</div>
<% } %>

<% if (vorgang.bemerkungen?.trim()) { %>
<hr>
<h2>Bemerkungen</h2>
<blockquote>
    <p class="mb-0">
        ${vorgang.bemerkungen.encodeAsRaw()}
    </p>
</blockquote>
<% } %>


<g:if test="${vorgang.begründung}">
    <hr>
    <h2>Begründung der Entscheidung</h2>
    <blockquote>
        <p class="mb-0">
            ${vorgang.begründung.encodeAsRaw()}
        </p>
    </blockquote>
</g:if>

<br>
<p>Link zum Vorgang:&nbsp;<a href="${params.from}" class="">${params.from}</a> </p>
<p style="page-break-after: always;">&nbsp;</p>