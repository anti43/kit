<!DOCTYPE html>
<g:set var="springSecurityService" bean="springSecurityService"/>
<html>

    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'vorgang.label', default: 'Vorgang')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#list-vorgang" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}">Startseite</a></li>
                <g:if test="${!(springSecurityService?.currentUser instanceof kit.Benutzer)}">
                    <li>
                        <a style="color:white" href="/vorgang/neu" class="btn btn-success">Neues Anliegen übermitteln</a>
                    </li>
                </g:if>
            </ul>
        </div>
        <div id="list-vorgang" class="content scaffold-list" role="main">
<g:if test="${!(springSecurityService?.currentUser instanceof kit.Benutzer)}">
            <h1>Öffentliche Vorgänge</h1>
</g:if><g:else>
            <h1>Öffentliche und Nicht-Öffentliche Vorgänge</h1>
        </g:else>
            <div class="row listrow">
                <div class="col-lg-4">
                    <div class="card my-4">
                        <h5 class="card-header">Suche</h5>

                        <div class="card-body">
                            <div class="input-group">
                                <g:form controller="vorgang" action="suche" class="form-inline">
                                    <input name="q" type="text" class="form-control" placeholder="Suche nach...">
                                    <button class="btn btn-info" type="submit">Los!</button>
                                </g:form>&nbsp;
                                <g:form controller="vorgang" action="liste" class="form-inline">
                                    <button class="btn btn-secondary" type="submit">Alle anzeigen</button>
                                </g:form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="card my-4">
                        <h5 class="card-header">Status</h5>

                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-10">
                                    <ul class="list-unstyled mb-0">

                                        <g:each in="${['Alle'] + kit.Vorgang.STATUS}">
                                            <li>
                                                <a href="/vorgang/status/${it}">${it}</a>
                                            </li>
                                        </g:each>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <!-- Categories Widget -->
                    <div class="card my-4">
                        <h5 class="card-header">Zuständigkeit</h5>

                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-10">
                                    <ul class="list-unstyled mb-0">

                                        <g:each in="${["Alle", "Gemeinderat", "Ortsrat", "Sonstiges"]}">
                                            <li>
                                                <a href="/vorgang/zustaendigkeit/${it}">${it}</a>
                                            </li>
                                        </g:each>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <g:if test="${flash.message}">

        <div class="row-fluid listrow">
            <div class="alert alert-info" role="status">${flash.message.encodeAsRaw()}</div>
        </div>
    </g:if>
    <div class="row-fluid listrow">
        <g:if test="${vorgangList}">
            <table class="data table table-striped table-bordered" style="width:100%">
                <thead>
                <th>Id</th>
                <th>Erstellt am</th>
                <th>Aktualisiert am</th>
                <th data-priority="1">Bezeichnung</th>
                <th>Status</th>
                <th>Zuständigkeit</th>
                <th>Öffentlich</th>
                <th>Anzeigen</th>
                </thead>
                <tbody>
                <g:each in="${vorgangList}">
                    <tr>
                        <td><a href="/vorgang/show/${it.id}">${it.id}</a></td>
                        <td>${it.dateCreated.format('dd.MM.yyyy')}</td>
                        <td>${it.lastUpdated.format('dd.MM.yyyy')}</td>
                        <td>${it.bezeichnung}</td>
                        <td>
                            <span class="badge badge-${it.getBadgeClass()}">
                                ${it.status}
                            </span>
                        </td>
                        <td>${it.werIstZustaendig ?: ''}</td>
                        <td>${it.oeffentlich ? 'Ja' : 'Nein'}</td>
                        <td><a class="btn btn-success" href="/vorgang/show/${it.id}">Anzeigen</a></td>
                    </tr>
                </g:each>
                </tbody>
            </table>

        </g:if>
    </div>
        </div>
<p>&nbsp;</p>
    </body>
</html>
